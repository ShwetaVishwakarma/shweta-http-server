const http = require('http');
const { v4: uuidv4 } = require('uuid');
//Setting up Port
const PORT = process.env.PORT || 4000
// Creating http Server
const httpServer = http.createServer(
    function (request, response) {
        try {
            let urlStatus = request.url.split("/");
            //console.log(urlStatus);
            switch (urlStatus[1]) {
                case "html":
                    const body = `< !DOCTYPE html>
                <html>
                  <head>
                  </head>
                  <body>
                      <h1>Any fool can write code that a computer can understand. Good programmers write code that humans can understand.</h1>
                      <p> - Martin Fowler</p>
                
                  </body>
                </html>`

                    // Calling response.writeHead method
                    response.writeHead(200, {
                        'Content-Type': 'text/html'
                    });
                    response.end(body);
                    break;
                case "json":
                    const object = `{
                        "slideshow": {
                          "author": "Yours Truly",
                          "date": "date of publication",
                          "slides": [
                            {
                              "title": "Wake up to WonderWidgets!",
                              "type": "all"
                            },
                            {
                              "items": [
                                "Why <em>WonderWidgets</em> are great",
                                "Who <em>buys</em> WonderWidgets"
                              ],
                              "title": "Overview",
                              "type": "all"
                            }
                          ],
                          "title": "Sample Slide Show"
                        }
                      }`

                    // Calling response.writeHead method
                    response.writeHead(200, {
                        'Content-Type': 'application/json'
                    });
                    response.end(object);
                    break;
                case "/uuid":
                    const uuidBody = { uuid: uuidv4() };

                    // Calling response.writeHead method
                    response.writeHead(200, {
                        'Content-Type': 'application/json'
                    });
                    response.end(JSON.stringify(uuidBody));
                    break;
                case "status":
                    // GET /status/{status_code} - Should return a response with a status code as specified in the request.
                    let statusCode = request.url.split("/");
                    console.log(statusCode);
                    let statusCodeLast = Number(statusCode[statusCode.length - 1]);
                    //console.log(typeof statusCodeLast);
                    //console.log(typeof http.STATUS_CODES);
                    if (http.STATUS_CODES[statusCodeLast] && statusCode.length === 3) {
                        response.writeHead(statusCode[statusCode.length - 1], {
                            'Content-Type': 'text/plain'
                        });
                        response.end(`${http.STATUS_CODES[statusCodeLast]}`);
                    } else {
                        throw new Error("422")
                    }
                    break;
                case "delay":
                    //GET /delay/{delay_in_seconds} - Should return a success response but after the specified delay in the request.
                    let delayTime = request.url.split("/")
                    //console.log(delayTime);
                    let delayTimeLastElememt = Number(delayTime[delayTime.length - 1]);
                    console.log(typeof delayTimeLastElememt);
                    if (delayTimeLastElememt && delayTime.length === 3 && typeof delayTimeLastElememt === "number") {
                        setTimeout(() => {
                            response.writeHead(200, {
                                'Content-Type': 'text/plain'
                            });
                            response.end(`Output is delayed by ${delayTimeLastElememt} second`);
                        }, delayTimeLastElememt * 1000);
                    } else {
                        throw new Error("422")
                    }
                    break;
                    default:
                        response.writeHead(404, {
                            'Content-Type': 'text/plain'
                        });
                        response.end("Url not found!!!");
            }
        } catch (err) {
            response.writeHead(402, {
                'Content-Type': 'text/plain'
            });
            response.end("Something Went Wrong!!");
        }
    });
// Listening to http Server
httpServer.listen(PORT,() => {
    console.log("Server is running at port 4000...");
});